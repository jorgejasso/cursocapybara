Given(/^I am on Edge Pipeline page$/) do
  visit 'https://www.edgepipeline.com/components/login'
end

When(/^I try to log in with "(.*?)" and "(.*?)"$/) do |user, pass|
  fill_in 'username', :with => user
  fill_in 'password', :with => pass
  click_on 'button_action'
end

Then(/^The home page is returned$/) do
  find('.header_avatar_menu').click
  page.has_content? ('Sign Out')
end

When(/^I try to log in with a wrong email or password$/) do
  fill_in 'username', :with => 'nil'
  fill_in 'password', :with => 'nil'
  click_on 'button_action'
end

Then(/^An error is returned$/) do
  page.has_content? ('Invalid Username or Password')
end

When(/^Touch Forgot Password$/) do
  click_link_or_button 'Forgot Password?'
end

Then(/^forgotten_password page is returned$/) do
  page.has_content? ('Reset Password')
end

When(/^Touch Sign up now$/) do
  click_link_or_button 'Sign Up Now!'
end

Then(/^signup page is returned$/) do
  page.has_content? ('Submit')
end
