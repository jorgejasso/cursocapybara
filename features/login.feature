Feature: Auction Edge Login
  Scenario: Sign in
    Given I am on Edge Pipeline page
    When I try to log in with "jorge.jasso@thincode.com" and "th1nc073"
    Then The home page is returned

  Scenario: Wrong email or password
    Given I am on Edge Pipeline page
    When I try to log in with a wrong email or password
    Then An error is returned

  Scenario: Forgot Password
    Given I am on Edge Pipeline page
    When Touch Forgot Password
    Then forgotten_password page is returned

  Scenario: Sign up now
    Given I am on Edge Pipeline page
    When Touch Sign up now
    Then signup page is returned
